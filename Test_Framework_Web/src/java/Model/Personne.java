/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitaire;

import java.util.HashMap;

/**
 *
 * @author antsa
 */
public class Personne {
    private String nom;
    private String prenom;
    private String age;

    public Personne() {
    }

    public Personne(String nom, String prenom, String age) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    
    
       
    @AnnotationUrl(url="formulaire.do")
    public static ModelView goFormulaire(){
        ModelView mv = new ModelView();
        HashMap<String, Object> hm =new HashMap<String, Object>();
        mv.setDatas(hm);
        mv.setUrl("formulaire.jsp");
        
        return mv;
    }
    
       
    @AnnotationUrl(url="inserer.do")
    public ModelView insertValue(){
        ModelView mv = new ModelView();
        
        HashMap<String, Object> hm =new HashMap<String, Object>();
        hm.put("nom", this.nom);   
        hm.put("prenom", this.prenom); 
        hm.put("age", this.age); 
        mv.setDatas(hm);
        mv.setUrl("resultat.jsp");
        
        return mv;
    }
    
    
    
}
