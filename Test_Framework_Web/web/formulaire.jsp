<%-- 
    Document   : formulaire
    Created on : 18 nov. 2022, 15:46:39
    Author     : antsa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Insérez</h1>
        
        <form action="inserer.do" method="post">
            <label>Nom : </label>
            <input name="nom" type="text" required/><br>
            
            <label>Prénoms : </label>
            <input name="prenom" type="text" required/><br>
            
            <label>Age : </label>
            <input name="age" type="number" required/><br>
            
            <button>Valider</button>
        </form>
        
    </body>
</html>
